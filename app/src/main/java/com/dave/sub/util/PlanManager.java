package com.dave.sub.util;

import android.content.Context;
import android.content.res.AssetManager;

import com.dave.sub.model.Bonus;
import com.dave.sub.model.Plan;
import com.dave.sub.model.Sms;
import com.dave.sub.model.Ussd;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Dave on 3/22/2018.
 */

public class PlanManager {

    private static Context context;

    private static final Random RANDOM = new Random();
    private static final List<Plan> PLANS = new LinkedList<>();

    public static void init(Context ctx) {
        context = ctx;
    }

    private static String loadXml() {
        String xml = "";
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;
        try {
            inputStream = assetManager.open("allplans.xml");

            StringBuilder builder = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append("\n");
            }
            return builder.toString();
        } catch (IOException e) {
            return xml;
        }
    }

    private static InputStream getXmlInputSource() {
        AssetManager assetManager = context.getAssets();
        try {
            InputStream inputStream = inputStream = assetManager.open("allplans.xml");
            return inputStream;
        } catch (IOException io) {
            return null;
        }
    }

    private static Document getXmlDocument() {
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        dbf.setCoalescing(true);
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(getXmlInputSource());
        } catch (ParserConfigurationException e) {
            return null;
        } catch (SAXException e) {
            return null;
        } catch (IOException e) {
            return null;
        }

        return doc;

    }

    private static List<Plan> parseXml() {
        List<Plan> plans = new LinkedList<>();
        Document document = getXmlDocument();
        if (document == null) {
            return plans;
        } else {
            document.normalizeDocument();
            Element element = document.getDocumentElement();
            NodeList nodeList = element.getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    NodeList childNodes = node.getChildNodes();
                    Plan plan = new Plan();
                    String sn = node.getAttributes().getNamedItem("sn").getNodeValue();
                    plan.setSn(Integer.parseInt(sn));
                    for (int j = 0; j < childNodes.getLength(); j++) {
                        Node childNode = childNodes.item(j);
                        if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                            if (childNode.getNodeName().equals("network")) {
                                plan.setNetwork(childNode.getTextContent().trim());
                            } else if (childNode.getNodeName().equals("amount")) {
                                plan.setAmount(Integer.parseInt(childNode.getTextContent().trim()));
                            } else if (childNode.getNodeName().equals("name")) {
                                plan.setName(childNode.getTextContent().trim());
                            } else if (childNode.getNodeName().equals("cost")) {
                                plan.setCost(Integer.parseInt(childNode.getTextContent().trim()));
                            } else if (childNode.getNodeName().equals("duration")) {
                                plan.setDuration(Integer.parseInt(childNode.getTextContent().trim()));
                            } else if (childNode.getNodeName().equals("sms")) {
                                NodeList grandChildrenNodes = childNode.getChildNodes();
                                Sms sms = new Sms();
                                for (int k = 0; k < grandChildrenNodes.getLength(); k++) {
                                    Node grandChild = grandChildrenNodes.item(k);
                                    if (grandChild.getNodeType() == Node.ELEMENT_NODE) {
                                        if (grandChild.getNodeName().equals("sub")) {
                                            String keyword = grandChild.getAttributes().getNamedItem("keyword").getNodeValue();
                                            sms.setSubKeyword(keyword);
                                            String no = grandChild.getAttributes().getNamedItem("no").getNodeValue();
                                            sms.setSubNumber(no);
                                        } else if (grandChild.getNodeName().equals("unsub")) {
                                            String keyword = grandChild.getAttributes().getNamedItem("keyword").getNodeValue();
                                            sms.setUnsubKeyword(keyword);
                                            String no = grandChild.getAttributes().getNamedItem("no").getNodeValue();
                                            sms.setUnsubNumber(no);
                                        } else if (grandChild.getNodeName().equals("balance")) {
                                            String keyword = grandChild.getAttributes().getNamedItem("keyword").getNodeValue();
                                            sms.setBalanceKeyword(keyword);
                                            String no = grandChild.getAttributes().getNamedItem("no").getNodeValue();
                                            sms.setBalanceNumber(no);
                                        }
                                    }
                                }
                                plan.setSms(sms);
                            } else if (childNode.getNodeName().equals("tarrif")) {
                                plan.setTarrif(childNode.getTextContent().trim());
                            } else if (childNode.getNodeName().equals("info")) {
                                plan.setInfo(childNode.getTextContent().trim());
                            } else if (childNode.getNodeName().equals("ussd")) {
                                NodeList grandChildrenNodes = childNode.getChildNodes();
                                Ussd ussd = new Ussd();
                                for (int k = 0; k < grandChildrenNodes.getLength(); k++) {
                                    Node grandChild = grandChildrenNodes.item(k);
                                    if (grandChild.getNodeType() == Node.ELEMENT_NODE) {
                                        if (grandChild.getNodeName().equals("sub")) {
                                            ussd.setSub(grandChild.getTextContent().trim());
                                        } else if (grandChild.getNodeName().equals("unsub")) {
                                            ussd.setUnsub(grandChild.getTextContent().trim());
                                        } else if (grandChild.getNodeName().equals("balance")) {
                                            ussd.setBalance(grandChild.getTextContent().trim());
                                        }
                                    }
                                }
                                plan.setUssd(ussd);
                            } else if (childNode.getNodeName().equals("bonus")) {
                                Bonus bonus = new Bonus();
                                bonus.setInfo(childNode.getTextContent().trim());
                                String amount = childNode.getAttributes().getNamedItem("amount").getNodeValue();
                                String percent = childNode.getAttributes().getNamedItem("percent").getNodeValue();
                                bonus.setAmount(Integer.parseInt(amount));
                                bonus.setPercent(Integer.parseInt(percent));
                                plan.setBonus(bonus);
                            }

                        }
                    }
                    plans.add(plan);
                }
            }
        }
        return plans;
    }

    public static void initList() {
        PLANS.clear();
        for (Plan plan : parseXml()) {
            PLANS.add(plan);
        }
    }

    public static List<Plan> getPlans() {
        return PLANS;
    }

    public static List<Plan> getByNetwork(String network) {
        List<Plan> tempPlans = new LinkedList<>();
        for (Plan plan : getPlans()) {
            if (network.equalsIgnoreCase(plan.getNetwork())) {
                tempPlans.add(plan);
            }
        }
        return tempPlans;
    }

    public static Set<Plan> getRandomPlans(String network, int number) {
        Set<Plan> uniquePlans = new HashSet<>();
        List<Plan> plansByNetwork = getByNetwork(network);

        while (number <= uniquePlans.size()) {
            uniquePlans.add(selectRandomPlan(plansByNetwork));
        }
        return uniquePlans;
    }

    public static List<Plan> getRandomPlan(String network, int number){
        Set<Plan> plan = getRandomPlans(network, number);
        List<Plan> planning =  new LinkedList<>(plan);
        return planning;
    }

    private static Plan selectRandomPlan(List<Plan> plans) {
        int index = RANDOM.nextInt(plans.size() - 1);
        return plans.get(index);
    }

    public static List<Plan> search(String network, String amount, String duration, String cost, String name, String tarrif) throws NoSuchFieldException, IllegalAccessException {
        List<Plan> results = new LinkedList<>();
        List<Plan> all = getPlans();
        String params[] = {network, amount, duration, cost, name, tarrif};
        String paramtype[] = {"network", "amount", "duration", "cost", "name", "tarrif"};
        for(Plan entry: all){
            boolean plan_fits = true;
            for(int i = 0; i < 6;){
                if((!(params[i].isEmpty()) && (Plan.class.getField(paramtype[i]).get(entry) == params[i])) || params[i].isEmpty()){
                    i++;
                }else{
                    i++;
                    plan_fits = false;
                }
            }
            if(plan_fits){ results.add(entry);}
        }
        return results;
    }

}
