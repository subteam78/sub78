package com.dave.sub.model;

import android.support.v4.util.Pair;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by prodigy4440 on 3/22/18.
 */

public class Sms implements Serializable {

    private String subKeyword;
    private String subNumber;

    private String unsubKeyword;
    private String unsubNumber;

    private String balanceKeyword;
    private String balanceNumber;

    public Sms() {
    }

    public Sms(String subKeyword, String subNumber, String unsubKeyword, String unsubNumber, String balanceKeyword,
               String balanceNumber) {
        this.subKeyword = subKeyword;
        this.subNumber = subNumber;
        this.unsubKeyword = unsubKeyword;
        this.unsubNumber = unsubNumber;
        this.balanceKeyword = balanceKeyword;
        this.balanceNumber = balanceNumber;
    }

    public String getSubKeyword() {
        return subKeyword;
    }

    public void setSubKeyword(String subKeyword) {
        this.subKeyword = subKeyword;
    }

    public String getSubNumber() {
        return subNumber;
    }

    public void setSubNumber(String subNumber) {
        this.subNumber = subNumber;
    }

    public String getUnsubKeyword() {
        return unsubKeyword;
    }

    public void setUnsubKeyword(String unsubKeyword) {
        this.unsubKeyword = unsubKeyword;
    }

    public String getUnsubNumber() {
        return unsubNumber;
    }

    public void setUnsubNumber(String unsubNumber) {
        this.unsubNumber = unsubNumber;
    }

    public String getBalanceKeyword() {
        return balanceKeyword;
    }

    public void setBalanceKeyword(String balanceKeyword) {
        this.balanceKeyword = balanceKeyword;
    }

    public String getBalanceNumber() {
        return balanceNumber;
    }

    public void setBalanceNumber(String balanceNumber) {
        this.balanceNumber = balanceNumber;
    }

    @Override
    public String toString() {
        return "Sms{" +
                "subKeyword='" + subKeyword + '\'' +
                ", subNumber='" + subNumber + '\'' +
                ", unsubKeyword='" + unsubKeyword + '\'' +
                ", unsubNumber='" + unsubNumber + '\'' +
                ", balanceKeyword='" + balanceKeyword + '\'' +
                ", balanceNumber='" + balanceNumber + '\'' +
                '}';
    }
}
