package com.dave.sub.model;

import java.io.Serializable;

/**
 * Created by prodigy4440 on 3/22/18.
 */

public class Ussd implements Serializable {
    private String sub;
    private String unsub;
    private String balance;

    public Ussd() {

    }

    public Ussd(String sub, String unsub, String balance) {
        this.sub = sub;
        this.unsub = unsub;
        this.balance = balance;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public String getUnsub() {
        return unsub;
    }

    public void setUnsub(String unsub) {
        this.unsub = unsub;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ussd)) return false;

        Ussd ussd = (Ussd) o;

        if (sub != null ? !sub.equals(ussd.sub) : ussd.sub != null) return false;
        if (unsub != null ? !unsub.equals(ussd.unsub) : ussd.unsub != null) return false;
        return balance != null ? balance.equals(ussd.balance) : ussd.balance == null;
    }

    @Override
    public int hashCode() {
        int result = sub != null ? sub.hashCode() : 0;
        result = 31 * result + (unsub != null ? unsub.hashCode() : 0);
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ussd{" +
                "sub='" + sub + '\'' +
                ", unsub='" + unsub + '\'' +
                ", balance=" + balance +
                '}';
    }
}
