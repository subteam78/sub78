package com.dave.sub.model;


import com.dave.sub.model.Bonus;
import com.dave.sub.model.Sms;
import com.dave.sub.model.Ussd;

import java.io.Serializable;

/**
 * Created by Dave on 3/20/2018.
 */

public class Plan implements Serializable{

    private int sn;
    public String name;
    public int amount;
    private Ussd ussd;
    private Sms sms;
    private Bonus bonus;
    public String network;
    private String info;
    public String tarrif;
    private int balance;
    public int duration;
    public int cost;

    public Plan() {
    }

    public Plan(int sn, String name, int amount, Ussd ussd, Sms sms, Bonus bonus, String network,
                String info, String tarrif, int balance, int duration, int cost) {
        this.sn = sn;
        this.name = name;
        this.amount = amount;
        this.ussd = ussd;
        this.sms = sms;
        this.bonus = bonus;
        this.network = network;
        this.info = info;
        this.tarrif = tarrif;
        this.balance = balance;
        this.duration = duration;
        this.cost = cost;
    }

    public int getSn() {
        return sn;
    }

    public void setSn(int sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Ussd getUssd() {
        return ussd;
    }

    public void setUssd(Ussd ussd) {
        this.ussd = ussd;
    }

    public Sms getSms() {
        return sms;
    }

    public void setSms(Sms sms) {
        this.sms = sms;
    }

    public Bonus getBonus() {
        return bonus;
    }

    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTarrif() {
        return tarrif;
    }

    public void setTarrif(String tarrif) {
        this.tarrif = tarrif;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString(){return name;}
}
