package com.dave.sub.model;

/**
 * Created by prodigy4440 on 3/22/18.
 */

public class Bonus {
    private String info;
    private int amount;
    private int percent;

    public Bonus() {
    }

    public Bonus(String info, int amount, int percent) {
        this.info = info;
        this.amount = amount;
        this.percent = percent;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return "Bonus{" +
                "info='" + info + '\'' +
                ", amount=" + amount +
                ", percent=" + percent +
                '}';
    }
}
